# Final Project

Welcome to the final project repository. This project contains SQL files for various assignments focusing on different aspects of database management and query operations.
## 1. Structure

The project is structured into several directories, each containing specific SQL files for different assignments:

- `pa1/`: Contains SQL files for the first assignment.
    - `schema.sql`: Includes schema-associated queries.
    - `queries.sql`: Includes CRUD operations queries.
    - `README.md`: Information about task.

- `pa2/`: Contains SQL files for the second assignment.
    - `schema_updates.sql`: Includes schema update queries.
    - `queries.sql`: Includes queries related to CRUD operations.
    - `README.md`: Information about task.
    - `pa2_bonus/`: Directory containing bonus task concerning indexes.
      - `bonus.pdf`: Includes bonus task explanation.
      - `pa2_bonus.sql`: Bonus task queries.

- `pa3/`: Contains SQL files for the third assignment.
    - `subqueries.sql`: Includes subquery-related queries.
    - `README.md`: Information about task.
    - `pa3_bonus/`: Directory containing a Python script for connecting to the database and performing CRUD operations.
      - `pa3_bonus.py`: Python script for bonus task.
      - `screenshots`: Screenshots that show output of CRUD operations.

- `pa4/`: Contains SQL files for the fourth assignment.
    - `procedures.sql`: Includes stored procedure queries.
    - `executions.sql`: Includes queries to execute stored procedures.
    - `README.md`: Information about task.
    - `pa4_bonus/`: Directory containing a Python script for connecting to the database and performing CRUD operations, handling transactions with error handling.
      - `pa4_bonus.py`: Python script for bonus task.
      - `output.png`: Screenshots that show output of CRUD operations.
- `pa5/`: Contains SQL files for the fifth assignment.
    - `views.sql`: Includes queries related to views.
    - `README.md`: Information about task.

## 2. How to deploy the project

To deploy the project, follow these steps:
1. **Clone the repository to your local machine:**
   ```
   git clone https://github.com/kshashkina/databaseFundamentals
   ```
2. **Navigate to the cloned repository directory:**
   ```
   cd databaseFundamentals
   ```
3. **Open your preferred SQL client (e.g., MySQL Workbench, pgAdmin, etc.).**
4. **Execute the SQL files in each directory sequentially:**
   - Start with the `pa1/` directory and progress to `pa5/`.
   - Read `README.md` provided in each directory for better understanding how to run various tasks.
     - [README](pa1/README.md) for the first task
     - [README](pa2/README.md) for the second task
     - [README](pa3/README.md) for the third task
     - [README](pa4/README.md) for the forth task
     - [README](pa5/README.md) for the fifth task
   - Use the appropriate commands in your SQL client to run the SQL files. For example, in MySQL Workbench, you can use the "Open SQL Script" option to open and execute each file.
5. **Ensure that you have the necessary database privileges:**
   - Make sure your database user has the privileges to create and modify schemas, tables, views, and procedures as required by the SQL files.

Following these steps will deploy the project and set up the necessary database schema and data.

## 3. How to run bonus tasks

Follow the instructions provided in the `README.md` file within the corresponding assignment directory.

## 4. Info about author

- **Author:** Kateryna Shashkina
- **Contact:** [kshashkina@kse.org.ua](mailto:kshashkina@kse.org.ua)
- **GitHub:** [kshashkina](https://github.com/kshashkina)
- **LinkedIn:** [Kateryna Shashkina](https://www.linkedin.com/in/kateryna-shashkina-4332bb291/)

Feel free to reach out if you have any questions or feedback regarding the project.

